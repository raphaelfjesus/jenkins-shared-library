import static com.domain.jenkins.JavaPipeline.*

def call(body) {
  def config= [:]
  body.resolveStrategy = Closure.DELEGATE_FIRST
  body.delegate = config
  body()

  def podLabel = "${config.projectName ?: getProjectName()}"

  /**
   * Before executing this pipeline, it is necessary to create the Persistent Volume Claim 
   * object called "maven-repo" in the Kubernetes cluster, because it will be used to store 
   * the dependencies downloaded by maven, being reused in the next executions of this pipeline 
   * and others that reference it. The following is an example of the contents of a Persistent 
   * Volume Claim:
   * 
   * <code>
   *   apiVersion: "v1"
   *   kind: "PersistentVolumeClaim"
   *   metadata: 
   *     name: "maven-repo"
   *     namespace: "jenkins"
   *   spec: 
   *     accessModes:
   *     - ReadWriteOnce
   *     resources:
   *       requests:
   *         storage: 10Gi
   *     storageClassName: glusterfs
   * </code>
   */
  pipeline {
    agent {
      kubernetes {
        label podLabel
        defaultContainer "ci-tools"
        yaml """
apiVersion: v1
kind: Pod
spec:
  containers:
  - name: ci-tools
    image: registry.local.com.br/infra/ci-tools:dind-jdk8-node11-alpine
    imagePullPolicy: IfNotPresent
    tty: true
    securityContext:
      privileged: true
    volumeMounts:
    - name: maven-repo
      mountPath: /root/.m2
    - name: dind-storage
      mountPath: /var/lib/docker
  imagePullSecrets:
  - name: registry-local
  volumes:
  - name: maven-repo
    persistentVolumeClaim:
      claimName: maven-repo
  - name: dind-storage
    emptyDir: {}
  hostAliases:
  - ip: 10.75.200.129
    hostnames:
      - sonar.local.com.br
      - nexus.local.com.br
      - registry.local.com.br
"""
      }
    }

    parameters {
      choice(
          name: "SONAR_EXPECTED_COVERAGE_PERCENTAGE",
          choices: [
            "75",
            "90",
            "100"
          ],
          description: "Minimum ratio for project testing coverage")
      string(
          name: "GATLING_USERS",
          defaultValue: "20",
          description: "Number of concurrent users that will be used in the performance test")
      string(
          name: "GATLING_RAMP_UP",
          defaultValue: "2",
          description: "Number of users that will be added over a period of time")
      string(
          name: "GATLING_DURATION",
          defaultValue: "60",
          description: "Maximum time allowed for performance test duration")
      string(
          name: "GATLING_THROUGHPUT",
          defaultValue: "100",
          description: "Number of requests per second")
      string(
          name: "GATLING_EXPECTED_RESPONSE_TIME",
          defaultValue: "2000000",
          description: "Expected response time to pass the performance test")
      choice(
          name: "GATLING_EXPECTED_SUCCESS_PERCENTAGE",
          choices: ["90", "75", "50", "25"],
          description: "Expected success percentage to pass the performance test")
    }

    environment {
      PROJECT_NAME = "${config.projectName ?: getProjectName()}"
      PROJECT_GROUP = "${config.projectGroup ?: 'dipol'}"
      PROJECT_HEALTH_URL = "${config.projectHealthUrl ?: 'localhost:8090/actuator/health'}"

      DOCKER_REGISTRY_URL = "https://registry.local.com.br"
      DOCKER_REGISTRY_DOMAIN = "${DOCKER_REGISTRY_URL}".replace("https://", "")
      DOCKER_REGISTRY_CREDENTIALS = "registry-token"
      DOCKER_REGISTRY_USERNAME = "jenkins-bot"
      DOCKER_REGISTRY_PASSWORD = credentials("${DOCKER_REGISTRY_CREDENTIALS}")

      SONAR_ENABLED = "true"
      SONAR_URL = "https://sonar.local.com.br"
      SONAR_CREDENTIALS = "sonar-token"
      SONAR_LOGIN = credentials("${SONAR_CREDENTIALS}")
      SONAR_PROJECT_KEY = "${PROJECT_NAME}"
      SONAR_QUALITY_GATE_API = "${SONAR_URL}/api/qualitygates/project_status?projectKey=${SONAR_PROJECT_KEY}"
      SONAR_COVERAGE_MEASURE_API = "${SONAR_URL}/api/measures/component?component=${SONAR_PROJECT_KEY}&metricKeys=coverage"

      SLACK_CHANNEL = "${config.slackChannel ?: '#build'}"
      SLACK_CREDENTIALS = "slack-token"

      GITLAB_CREDENTIALS = "gitlab-token"
      GITLAB_TOKEN = credentials("${GITLAB_CREDENTIALS}")
      GITLAB_URL = "http://gitlab.prodesp.sp.gov.br"

      RELEASE_BRANCH_NAME = "master"
      RELEASE_TAG_PREFIX = "v"
      RELEASE_IMAGE_NAME = "${DOCKER_REGISTRY_DOMAIN}/${PROJECT_GROUP}/${PROJECT_NAME}"
      RELEASE_IMAGE_PREFIX = "release-"
      PREVIEW_IMAGE_PREFIX = "preview-"

      COMMIT_URL = "${getCommitUrl()}"
      COMMIT_HASH = "${getShortCommitHash()}"
      COMMIT_AUTHOR_NAME = "${getChangeAuthorName()}"
      COMMIT_AUTHOR_EMAIL = "${getChangeAuthorEmail()}"
      
      CI_TOOLS_IMAGE = "registry.local.com.br/infra/ci-tools:dind-jdk8-node11-alpine"
    }

    options {
      timeout time: 60, unit: "MINUTES"
      buildDiscarder(
          logRotator(
          artifactDaysToKeepStr: "",
          artifactNumToKeepStr: "",
          daysToKeepStr: "10",
          numToKeepStr: "20"))
      gitLabConnection("gitlab")
      gitlabBuilds(
          builds: [
            "Checkout",
            "Verify Tools",
            "Build Artifact",
            "Unit Tests",
            "Integration Tests",
            "Sonar Analysis",
            "Build Image",
            "Test Image",
            /*"Performance Analysis",*/
            "Push Image",
            "Release Image & Tag"
          ])
    }
    
    triggers {
      gitlab(
        triggerOnPush: false,
        triggerOnMergeRequest: true,
        triggerOpenMergeRequestOnPush: "never",
        triggerOnNoteRequest: true,
        noteRegex: "Jenkins please retry a build",
        skipWorkInProgressMergeRequest: true,
        ciSkip: false,
        setBuildDescription: true,
        addNoteOnMergeRequest: true,
        addCiMessage: true,
        addVoteOnMergeRequest: true,
        acceptMergeRequestOnSuccess: false,
        branchFilterType: "NameBasedFilter",
        includeBranchesSpec: "master",
        excludeBranchesSpec: "",
        pendingBuildName: "Jenkins",
        cancelPendingBuildsOnUpdate: true)
    }

    stages {

      /**
       * Clone the source code of the repository configured for the pipeline.
       */
      stage("Checkout") {
        steps {
          gitlabCommitStatus(env.STAGE_NAME) {
            // The 'checkout scm' alway run in the start pipeline
          }
        }
      }

      /**
       * Checks if the tools needed to run the pipeline are available in the pipeline 
       * execution environment.
       */
      stage("Verify Tools") {
        steps {
          gitlabCommitStatus(env.STAGE_NAME) {
            sh """
              docker --version
              node --version
              npm --version
              mvn --version
              git --version
              jq --version
              semantic-release --version
            """
          }
        }
      }

      /**
       * Builds the artifact (.jar) of the project, making it available for the
       * next stages of the pipeline.
       */
      stage("Build Artifact") {
        steps {
          gitlabCommitStatus(env.STAGE_NAME) {
            sh "mvn -B clean package -DskipTests"
          }
        }
        post {
          success {
            archiveArtifacts artifacts: "**/target/*.jar", allowEmptyArchive: true
          }
        }
      }

      /**
       * Run the tests implemented for the project, based on the artifact built in 
       * the previous step.
       */
      stage("Test Artifact") {
        when {
          expression { true }
        }
        stages {
          stage("Unit Tests") {
            steps {
              gitlabCommitStatus(env.STAGE_NAME) {
                sh "mvn -B verify -DskipIntegrationTests=true"
              }
            }
            post {
              always {
                junit testResults: "**/target/surefire-reports/*.xml", allowEmptyResults: true
              }
            }
          }
          stage("Integration Tests") {
            steps {
              gitlabCommitStatus(env.STAGE_NAME) {
                sh """
                  docker login --username=${env.DOCKER_REGISTRY_USERNAME} --password=${env.DOCKER_REGISTRY_PASSWORD} ${env.DOCKER_REGISTRY_URL} \
                  && mvn -B verify -DskipUnitTests=true
                """
              }
            }
            post {
              always {
                junit testResults: "**/target/failsafe-reports/*.xml", allowEmptyResults: true
              }
            }
          }
        }
      }

      /**
       * Scans the application source code for security vulnerabilities, bugs, source code
       * duplicity, and other quality metrics.
       */
      stage("Sonar Analysis") {
        when {
          expression { env.SONAR_ENABLED }
        }
        steps {
          gitlabCommitStatus(env.STAGE_NAME) {
            sh """
              mvn -B sonar:sonar \
                  -DskipTests \
                  -Dsonar.host.url=${env.SONAR_URL} \
                  -Dsonar.login=${env.SONAR_LOGIN} \
                  -Dsonar.projectKey=${env.SONAR_PROJECT_KEY} \
                  -Dsonar.jacoco.reportPaths=target/coverage-reports/jacoco-it.exec,target/coverage-reports/jacoco-ut.exec \
                  -Dsonar.junit.reportPaths=target/surefire-reports,target/failsafe-reports
            """

            script {
              def qualityGate = getQualityGateStatus()
              echo "Quality Gate ${qualityGate}"

              if (qualityGate.equalsIgnoreCase("ERROR")) {
                error "Pipeline aborted due to quality gate failure"
              }

              def coverage = getCoveragePercentage()
              echo "Coverage ${coverage.toString()}%"

              if (coverage < new Double(params.SONAR_EXPECTED_COVERAGE_PERCENTAGE)) {
                error "Pipeline aborted due to coverage failure"
              }
            }
          }
        }
      }

      /**
       * Builds the application's docker image based on the artifact previously built.
       */
      stage("Build Image") {
        steps {
          gitlabCommitStatus(env.STAGE_NAME) {
            sh """
              docker login --username=${env.DOCKER_REGISTRY_USERNAME} --password=${env.DOCKER_REGISTRY_PASSWORD} ${env.DOCKER_REGISTRY_URL} \
              && docker build --tag ${env.RELEASE_IMAGE_NAME}:${getImageVersion()} --build-arg JAR_FILE=${getJarFile()} .
            """
          }
        }
      }

      /**
       * Run a container from the constructed image and verifies that the application
       * is healthy, from a check URL.
       */
      stage("Test Image") {
        steps {
          gitlabCommitStatus(env.STAGE_NAME) {
            script {            
              timeout(time: 1, unit: "MINUTES") {
                docker.image("${env.RELEASE_IMAGE_NAME}:${getImageVersion()}").inside {
                  waitUntil {
                    try {
                      sleep 5
                      return sh(returnStdout: true, script: "curl --silent ${env.PROJECT_HEALTH_URL} | jq --raw-output '.status'").trim() == "UP"
                    } catch(error) {
                      return false
                    }
                  }
                }
              }
            }
          }
        }
      }

      /**
       * Run the application performance test, ensuring that the image settings are
       * adhering to the project needs.
       */
      /*stage("Performance Analysis") {
       environment {
       GATLING_URL = "http://application:8090"
       }
       steps {
       gitlabCommitStatus(env.STAGE_NAME) {
       script {
       docker.image("${env.RELEASE_IMAGE_NAME}:${getImageVersion()}").withRun { c ->
       docker.image(env.CI_TOOLS_IMAGE).inside("--link ${c.id}:application -v $HOME/.m2:/root/.m2") {
       sh """
       mvn -B gatling:test \
       -Dbaseurl=${env.GATLING_URL} \
       -Dsimulation=ExampleSimulation \
       -Dusers=${params.GATLING_USERS} \
       -Drampup=${params.GATLING_RAMP_UP} \
       -Dduration=${params.GATLING_DURATION} \
       -Dthroughput=${params.GATLING_THROUGHPUT} \
       -Dresponsetime=${params.GATLING_EXPECTED_RESPONSE_TIME} \
       -Dsuccesspercentage=${params.GATLING_EXPECTED_SUCCESS_PERCENTAGE}
       """
       }
       }
       }
       }
       }
       }*/

      /**
       * Provides the Docker image of the bundled and tested application to the Docker
       * Registry configured for the project.
       */
      stage ("Push Image") {
        steps {
          gitlabCommitStatus(env.STAGE_NAME) {
            sh """
              docker login --username=${env.DOCKER_REGISTRY_USERNAME} --password=${env.DOCKER_REGISTRY_PASSWORD} ${env.DOCKER_REGISTRY_URL} \
              && docker push ${env.RELEASE_IMAGE_NAME}:${getImageVersion()}
            """
          }
        }
      }

      /**
       * It provides the docker image for deployment in the staging and production
       * environment, and generates a tag and submits it to the source code repository.
       */
      stage("Release Image & Tag") {
        when {
          expression { env.BRANCH_NAME == env.RELEASE_BRANCH_NAME }
        }
        steps {
          gitlabCommitStatus(env.STAGE_NAME) {
            sh """
              export GITLAB_TOKEN="${env.GITLAB_TOKEN}"
              export GITLAB_URL="${env.GITLAB_URL}"
              export GIT_AUTHOR_NAME=jenkins-bot
              export GIT_AUTHOR_EMAIL=jenkins-bot@example.org
              export GIT_COMMITTER_NAME=jenkins-bot
              export GIT_COMMITTER_EMAIL=jenkins-bot@example.org
              echo '
              {
                "branch": "${env.RELEASE_BRANCH_NAME}",
                "plugins": [
                  "@semantic-release/commit-analyzer",
                  "@semantic-release/release-notes-generator",
                  ["@semantic-release/changelog", {
                    "changelogFile": "CHANGELOG.md",
                    "changelogTitle": "Changelog"
                  }],
                  ["@semantic-release/exec", {
                    "prepareCmd": "
                       mvn org.codehaus.mojo:versions-maven-plugin:2.7:set -DnewVersion=\${nextRelease.version} -DgenerateBackupPoms=false \
                       && docker login --username=${env.DOCKER_REGISTRY_USERNAME} --password=${env.DOCKER_REGISTRY_PASSWORD} ${env.DOCKER_REGISTRY_URL} \
                       && docker pull ${env.RELEASE_IMAGE_NAME}:${getImageVersion()} \
                       && docker tag ${env.RELEASE_IMAGE_NAME}:${getImageVersion()} ${env.RELEASE_IMAGE_NAME}:\${nextRelease.version} \
                       && docker push ${env.RELEASE_IMAGE_NAME}:\${nextRelease.version}"
                  }],
                  ["@semantic-release/git", {
                    "assets": [
                      "pom.xml",
                      "CHANGELOG.md"
                    ]
                  }],
                  ["@semantic-release/gitlab", {
                    "assets": []
                  }]
                ]
              }
              ' >> .releaserc
              npx semantic-release --no-ci
            """
          }
        }
      }
    }
    
    post {
      success {
        echo "success"
        /*sendNotification("Successful")*/
      }
  
      failure {
        echo "failure"
        /*sendNotification("Failure")*/
      }
    }
  }
}

/**
 * Returns the repository url. For example:
 *
 * https://gitlab.com/raphaelfjesus/demo-spring-boot
 */
def getRepositoryUrl() {
  return sh(returnStdout: true, script: "git config remote.origin.url").trim().replace(".git", "")
}

/**
 * Returns the full hash of the commit. For example:
 *
 * Full Hash: d82b43f315f39384df9fbd5df70ae1139b80fe4c
 */
def getFullCommitHash() {
  return sh(returnStdout: true, script: "git log -n 1 --pretty=format:'%H'").trim()
}

/**
 * Returns the short hash of the commit. For example:
 *
 * Full Hash: d82b43f315f39384df9fbd5df70ae1139b80fe4c
 * Short Hash: d82b43f
 */
def getShortCommitHash() {
  return getFullCommitHash().take(7)
}

/**
 * Returns the commit URL that triggered the pipeline. For example:
 *
 * https://gitlab.com/raphaelfjesus/demo-spring-boot/commit/d82b43f315f39384df9fbd5df70ae1139b80fe4c
 */
def getCommitUrl() {
  return "${getRepositoryUrl()}/commit/${getFullCommitHash()}"
}

/**
 * Returns the name of the commit author who triggered the pipeline. For example:
 *
 * Raphael F. Jesus
 */
def getChangeAuthorName() {
  return sh(returnStdout: true, script: "git show -s --pretty=%an").trim()
}

/**
 * Returns the email of the commit author who triggered the pipeline. For example:
 *
 * raphaelfjesus@gmail.com
 */
def getChangeAuthorEmail() {
  return sh(returnStdout: true, script: "git show -s --pretty=%ae").trim()
}

/**
 * Returns the jar file built for the project. For example:
 *
 * target/demo-spring-boot-1.0.0.jar
 */
def getJarFile() {
  return sh(returnStdout: true, script: "ls target/*.jar | sort -n | head -1").trim()
}

/**
 * Returns the version for the project. For example:
 *
 * For Release -> release-06f5577
 * For Preview -> preview-06f5577
 */
def getImageVersion() {
  return (env.BRANCH_NAME == env.RELEASE_BRANCH_NAME ? env.RELEASE_IMAGE_PREFIX : env.PREVIEW_IMAGE_PREFIX) + env.COMMIT_HASH
}

/**
 * Returns the Quality Gate value assigned to the project after Sonar analysis. For example:
 *
 * OK = for passed
 * ERROR = for failure
 */
def getQualityGateStatus() {
  return sh(returnStdout: true, script: "curl --silent --insecure --user ${env.SONAR_LOGIN}: '${env.SONAR_QUALITY_GATE_API}' | jq -r '.projectStatus.status'").trim()
}

/**
 * Returns the Coverage percentage assigned to the project after Sonar analysis. For example:
 *
 * 100 = all covered tests
 * 77.9 = partially covered tests
 */
def getCoveragePercentage() {
  def coverage = sh(returnStdout: true, script: "curl --silent --insecure --user ${env.SONAR_LOGIN}: '${env.SONAR_COVERAGE_MEASURE_API}' | jq -r '.component.measures | .[0].value'").trim()
  return new Double(coverage)
}

/**
 * Returns the project name. For example:
 *
 * Variable "currentBuild.fullProjectName" equals "demo-spring-boot/master" then
 *
 * return "demo-spring-boot"
 */
def getProjectName() {
  return "${currentBuild.fullProjectName}".split("/")[0].trim()
}

def sendNotification(String buildStatus = "Started") {
  def color = buildStatus == "Successful" ? "#00FF00" : (buildStatus == "Failure" ? "#FF0000" : "FFFF00")
  def subject = "${env.JOB_NAME} | Build #${currentBuild.number} - " + buildStatus + " for ${env.BRANCH_NAME}"
  def message = """
*Build* <${env.BUILD_URL}|#${currentBuild.number}> - ${buildStatus} for branch ${env.BRANCH_NAME}
*Commit Hash* <${env.COMMIT_URL}|${env.COMMIT_HASH}>
*Commit Author* <mailto:${env.COMMIT_AUTHOR_EMAIL}|${env.COMMIT_AUTHOR_NAME}>
  """

  slackSend channel: env.SLACK_CHANNEL, color: color, message: message, botUser: true, tokenCredentialId: env.SLACK_CREDENTIALS
}
